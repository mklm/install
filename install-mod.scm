(module install
  (install
   install-directory
   install-file
   install-usage
   install-version
   file=?)
  (import
    scheme
    chicken
    foreign)
  (use
    files
    posix)
  (include "install-version.scm")
  (include "install.scm"))
