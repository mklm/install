(use test install extras srfi-13)

(define num-len 2)
(define command-max-len (- 68 num-len))

(define (cmd-str cmd num)
  (let ((str (irregex-replace/all "  *"
               (string-append (string-pad (number->string num) num-len #\0)
                              " "
                              (string-delete #\newline (with-output-to-string
                                                         (lambda () (pp cmd)))))
               " ")))
    (substring str 0 (min command-max-len (string-length str)))))

(define (check-objs testf cleanf . o)
  (let loop ((objs o) (ok? #t))
    (if (null? objs)
      ok?
      (let ((obj (car objs)))
        (if (testf obj)
          (begin
            (cleanf obj)
            (loop (cdr objs) ok?))
          (loop (cdr objs) #f))))))

(define-syntax dtest
  (syntax-rules ()
    ((_ num cmd dir1 dir2 ...)
     (begin
       cmd
       (test (cmd-str 'cmd num)
             #t
             (check-objs directory? delete-directory dir1 dir2 ...))))))

(define-syntax ftest
  (syntax-rules ()
    ((_ num cmd file1 file2 ...)
     (begin
       cmd
       (test (cmd-str 'cmd num)
             #t
             (check-objs file-exists? delete-file file1 file2 ...))))))

(define-syntax etest
  (syntax-rules ()
    ((_ num cmd)
     (test-error (cmd-str 'cmd num) cmd))))

(create-directory "tmp")

 (ftest 01 (install-file "../README.md" "tmp") "tmp/README.md")
 (ftest 02 (install-file '("../README.md") "tmp") "tmp/README.md")
 (ftest 03 (install-file '("../README.md" "../install.meta") "tmp")  "tmp/README.md" "tmp/install.meta")
 (etest 04 (install-file '("../README.md" "../install.meta") "tmp/a"))
 (dtest 05 (install-directory "tmp/a") "tmp/a")
 (dtest 06 (install-directory (list "tmp/a")) "tmp/a")
 (dtest 07 (install-directory (list "tmp/a" "tmp/b")) "tmp/a" "tmp/b")
 (etest 08 (install))
 (etest 09 (install -d))
 (etest 10 (install "tmp/run.scm"))
 (etest 11 (install -z "../README.md" "tmp/"))
 (etest 12 (install "tmp" "toasts"))
 (etest 13 (install -d "run.scm"))
 (etest 14 (install "install.meta" "install.meta"))
 (dtest 15 (install -d "tmp/a") "tmp/a")
 (dtest 16 (install -d "tmp/a" "tmp/b") "tmp/a" "tmp/b")
 (ftest 17 (install "../README.md" "tmp") "tmp/README.md")
 (ftest 18 (install "../README.md" "../install.meta" "tmp/") "tmp/README.md" "tmp/install.meta")
 (ftest 19 (install "run.scm" "tmp/rum.scm") "tmp/rum.scm")
 (ftest 20 (install (string-append "../README" ".md") "tmp/") "tmp/README.md")
(cond-expand
(mingw32
 (ftest 21 (install "nul" "tmp/a") "tmp/a")
 (etest 22 (install "nul" "tmp/")))
(else
 (ftest 21 (install "/dev//null" "tmp/a") "tmp/a")
 (etest 22 (install "/dev/null" "tmp/")))
)
 (ftest 23 (begin (install "../README.md" "tmp/a") (install "../README.md" "tmp/a")) "tmp/a")
 (etest 24 (install -m 855 "../README.md" "tmp/"))
 (ftest 25 (install -m 644 "../README.md" "tmp/") "tmp/README.md")
 (etest 26 (install -p -d "tmp/a"))
 (ftest 27 (install -p "../README.md" "tmp/") "tmp/README.md")
 (etest 28 (install -d -d "tmp/a"))
 (etest 29 (install -p -p "../README.md" "tmp/"))
 (etest 30 (install -m 644 -m 644 "../README.md" "tmp/"))
 (etest 31 (install -g 0 -g 0 "../README.md" "tmp/"))
 (etest 32 (install -o 0 -o 0 "../README.md" "tmp/"))

(delete-directory "tmp")

(test-exit)
