# install

**install** is a ([CHICKEN](http://www.call-cc.org)) Scheme implementation of
the Unix utility *install*.

-------------

```
#!scheme
[procedure] (install-file FILES TARGET #!key OWNER GROUP (MODE 755) PRESERVE-TIME)
```

File(s) are copied to the target file or directory.  If the destination is a
directory, then the file is copied into the directory with its original
filename.  If the target file already exists, it is overwritten if permissions
allow. *install-file* attempts to prevent installing a file onto itself.
Installing */dev/null* (*nul* on Windows) creates an empty file. *mode* must be
expressed in octal notation; *mode*, *owner* and *group* are ignored on
Windows. The return value is unspecified.

-------------

```
#!scheme
[procedure] (install-dir DIRECTORIES #!key OWNER GROUP (MODE 755))
```

Create all components of the given directory(ies). *mode* must be expressed in
octal notation; *mode*, *owner* and *group* are ignored on Windows. The return
value is unspecified.

-------------

```
#!scheme
[syntax] (install [-p] [-g GROUP] [-m MODE] [-o OWNER] SRC DST)
[syntax] (install [-p] [-g GROUP] [-m MODE] [-o OWNER] SRC ... DIR)
[syntax] (install      [-g GROUP] [-m MODE] [-o OWNER] -d DIR ...)
```

Handy command-line-like syntax around *install-file* and *install-directory*.

-------------

```
#!scheme
[procedure] (file=? FILE1 FILE2)
```
Determine whether two paths represents the same file.

-------------

## Installation

```
#!sh
$ chicken-install install
```

Optionally a command-line utility (*instll*) is also available.

```
#!sh
$ chicken-install install -D install-cmd
```

```
#!sh
$ instll
Usage:

  instll [-p] [-g GROUP] [-m MODE] [-o OWNER] SRC DST
  instll [-p] [-g GROUP] [-m MODE] [-o OWNER] SRC ... DIR
  instll [-g GROUP] [-m MODE] [-o OWNER] -d DIR ...

  -g      Set group ownership, instead of process' current group.

  -m      Set permission mode instead of rwxr-xr-x (755). The mode must
          be expressed in octal notation.

  -o      Set ownership (super-user only).

  -p      Preserve timestamps. Apply modification time of SRC files to
          corresponding destination files.
```

## Usage

```
#!scheme
(use install)

(install -?)
(install -d "/tmp/tests")
(install -d "C:\\tmp\\tests")
(install -d "C:/tmp/tests")
(install -d "/tmp/tests/a" "/tmp/tests/b")
(install -d -m 644 "/tmp/tests")

(install -m 644 "file1" "/tmp/tests")
(install -m 644 "file1" "file2")
(install -m 644 "file1" "file2" "/tmp/tests")
(install -o "mikele" -m 644 "file1" "/tmp/tests")
(install -m 644 -p "file1" "/tmp/tests")
(install "/dev/null" "/usr/bin/empty")
(install "nul" "C:/bin/empty")

(install-file "file1" "/tmp/tests")
(install-file '("file1") "/tmp/tests")
(install-file '("file1" "file2") "/tmp/tests")
(install-file (glob "file*") "/tmp/tests")
(install-file "file1" "/tmp/tests" mode: 644)

(install-directory "/tmp/tests")
(install-directory '("/tmp/tests"))
(install-directory '("/tmp/tests/a" "/tmp/tests/b"))
(install-directory "/tmp/tests" group: 0 owner: "mikele")

(begin (create-symbolic-link "vi" "emacs")
       (file=? "emacs" "vi"))                                ;; #t
(begin (change-directory "C:\\Windows")
       (file=? "C:\\Windows\\notepad.exe" "notepad.exe"))    ;; #t
```

## Requirements

None. Windows (mingw32) users need to build CHICKEN from git (or a stability 
version > 4.9.0.1).

## About this software

The source code is hosted at [Bitbucket](https://bitbucket.org/mklm/install/).
Feel free to send pull requests or open an issue there. Alternatively, send an
e-mail to the [chicken-users](mailto:chicken-users@nongnu.org) mailing list for
information or requests.

### Author

[Michele La Monaca](mailto:install-scheme@lamonaca.net)

### Version history

**1.0.1** Initial release

### License

```
#!text
Copyright (c) 2014
Michele La Monaca (install-scheme@lamonaca.net)
All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:

 1) Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 2) Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.

 3) Neither the name of the author nor the names of its contributors
 may be used to endorse or promote products derived from this software
 without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 OF THE POSSIBILITY OF SUCH DAMAGE.
```
