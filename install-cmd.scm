;;; Copyright (c) 2014
;;  Michele La Monaca (install-scheme@lamonaca.net)
;;; All rights reserved.

(use install)
(use irregex)

(define *owner* #f)
(define *group* #f)
(define *mode* "755")
(define *preserve-time* #f)

(define (usage)
  (display
    (irregex-replace/all "\\(install (.*)\\)" (install-usage) "instll " 1)
    (current-error-port)))

(let ((no-more-options #f) (install-list '()) (install-dirs? #f))
  (let loop ((args (command-line-arguments)))
    (if (null? args)
      (cond ((null? install-list) (usage))
            (install-dirs?
              (install-directory install-list
                                 mode: (or (string->number *mode*) 8)
                                 owner: *owner*
                                 group: *group*
                                 preserve-time: *preserve-time*))
            ((null? (cdr install-list)) (usage))
            (else
              (install-file (cdr install-list)
                            (car install-list)
                            mode: (or (string->number *mode*) 8)
                            owner: *owner*
                            group: *group*
                            preserve-time: *preserve-time*)))
      (let ((arg (car args)))
        (cond ((string=? arg "-d")
               (if no-more-options (usage))
               (set! install-dirs? #t)
               (loop (cdr args)))
              ((string=? arg "-p")
               (if no-more-options (usage))
               (set! *preserve-time* #t)
               (loop (cdr args)))
              ((string=? arg "-m")
               (if no-more-options (usage))
               (if (pair? (cdr args))
                 (set! *mode* (cadr args))
                 (usage))
               (loop (cddr args)))
              ((string=? arg "-o")
               (if no-more-options (usage))
               (if (pair? (cdr args))
                 (set! *owner* (cadr args))
                 (usage))
               (loop (cddr args)))
              ((string=? arg "-g")
               (if no-more-options (usage))
               (if (pair? (cdr args))
                 (set! *group* (cadr args))
                 (usage))
               (loop (cddr args)))
              (else
                (set! no-more-options #t)
                (set! install-list (cons arg install-list))
                (loop (cdr args))))))))
